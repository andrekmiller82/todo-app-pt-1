import React, { Component } from 'react';
import todosList from './todos.json';

class App extends Component {
  state = {
    todos: todosList,
    value: '',
  };

  handleTodo = (event) => {
    if(event.key ==="Enter"){

      const addTodo = {
        userId: 1,
        id:Math.random()*1000,
        title: event.target.value,
        completed: false,
      }
      let updateAddTodo=[...this.state.todos, addTodo];
      this.setState({...this.state, todos:updateAddTodo,value:""})
    }
    
  };


  handleSubmit=(event)=>{

    this.setState({...this.state,value:event.target.value})
  }


  handleCheckBox=(id)=>{
    let checked = this.state.todos.map((todo)=>{
      if(todo.id===id){
        return {
          ...todo, completed:!todo.completed
        }
      }
      return todo
    })
    this.setState((state)=>{
      return{
        ...state, todos:checked
      }
    })
  }

  handleDelete =(event,id)=>{
    let deleteLine = this.state.todos.filter((todo)=>todo.id !== id)
    this.setState({todos:deleteLine})
  }
  
  handleDeleteAll =(event)=>{
    let deleteall=this.state.todos.filter((todo)=>todo.completed===false)
    this.setState({todos:deleteall})
  }
  
  render() {
    return (
      <section className='todoapp'>
        <header className='header'>
          <h1>todos</h1>
          
            <input
              className='new-todo'
              type='text'
              placeholder='What needs to be done?'
              value={this.state.value}
              onChange={this.handleSubmit}
              onKeyDown={this.handleTodo}
              autoFocus
            />
          
        </header>

        <TodoList
          todos={this.state.todos}
          handleDelete={this.handleDelete}
          handleCheckBox={this.handleCheckBox}
        />
        <footer className='footer'>
          <span className='todo-count'>
            <strong>0</strong> item(s) left
          </span>
          <button className='clear-completed' onClick={this.handleDeleteAll}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleCheckBox}
          />

          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={this.props.handleDelete}
          />
        </div>
      </li>
    );
  }
}


class TodoList extends Component {


  render() {
    return (
      <section className="main">
      
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
            key={todo.id}
              title={todo.title}
              completed={todo.completed}
             handleCheckBox={(event)=>this.props.handleCheckBox(todo.id)}
             handleDelete={(event)=>this.props.handleDelete(event, todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
